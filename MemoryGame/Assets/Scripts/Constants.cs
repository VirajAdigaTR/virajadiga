﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour
{
    public const int mEasy=16;
    public const int mIntermediate=20;
    public const int mDifficult=24;
    public const int mEasyMoves=64;
    public const int mIntermediateMoves=60;
    public const int mDifficultMoves=50;
    public const int mEasyHint=3;
    public const int mIntermediateHint=2;
    public const int mDifficultHint=1;
    public static bool mEasyChoice=false,mIntermediateChoice=false,mDifficultChoice=false;

    void Awake(){
      mEasyChoice=false;
      mIntermediateChoice=false;
      mDifficultChoice=false;
   }
   public void easy(){
       mEasyChoice=true;
       mIntermediateChoice=false;
       mDifficultChoice=false;
       Application.LoadLevel("Game");
   } 
   public void intermediate(){
       mEasyChoice=false;
       mIntermediateChoice=true;
       mDifficultChoice=false;
        Application.LoadLevel("Game");
   } 
   public void difficult(){
       mEasyChoice=false;
       mIntermediateChoice=false;
       mDifficultChoice=true;
        Application.LoadLevel("Game");
   } 
   public void quit(){
       Application.Quit();
   }
}
